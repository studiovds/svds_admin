var gulp = require('gulp'),
    sass = require('gulp-sass'),
    prefix = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    cleancss = require('gulp-clean-css'),
    debug = require('gulp-debug');


function buildScriptsAdmin(done) {
    var filename = 'style.css';
    gulp.src(['css/style.scss'])
        .pipe(concat(filename))
        .pipe(debug({title: 'filename:'}))
        .pipe(sass())
        .pipe(prefix(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
        .pipe(cleancss())
        .pipe(gulp.dest('build'));
    filename = 'custom-client.css';
    gulp.src(['css/custom-client.scss'], { allowEmpty: true })
        .pipe(concat(filename))
        .pipe(debug({title: 'filename:'}))
        .pipe(sass())
        .pipe(prefix(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
        .pipe(cleancss())
        .pipe(gulp.dest('build'));
    filename = 'ckeditor.css';
    gulp.src(['css/ckeditor.scss'], { allowEmpty: true })
        .pipe(concat(filename))
        .pipe(debug({title: 'filename:'}))
        .pipe(sass())
        .pipe(prefix(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
        .pipe(cleancss())
        .pipe(gulp.dest('build'));
    //CLEAR CACHE
    clearCache();
    done();
}

function clearCache(){
    shell.task([
        'drush cr'
    ]);
}

/**
 * @task watch
 * Watch scss files for changes & recompile
 * Clear cache when Drupal related files are changed
 */
gulp.task('watch', function () {
    gulp.watch(['{css,js}/*.*'], buildScripts);
    gulp.watch(['../svds_admin/css/*.*'], buildScriptsAdmin);
    gulp.watch(['tpl/*/*.twig', '*.{yml,php}', '../../../sites/default/*.{php,yml}', '../../../sites/*.{php,yml}'], clearCache);
});

/**
 * Default task, running just `gulp` will
 * compile Sass files, launch watch files.
 */
gulp.task('default', gulp.series('watch'));